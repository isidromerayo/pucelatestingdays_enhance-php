<?php
namespace Agilecyl;

/**
 *
 * @author isidromerayo
 */
interface ServiceInterface {
    /**
     * Read temperature
     */
    public function readTemp();
}
