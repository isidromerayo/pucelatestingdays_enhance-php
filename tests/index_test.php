<?php
require __DIR__  . '/../vendor/autoload.php';

// Find the tests - '.' is the current folder
\Enhance\Core::discoverTests(__DIR__);
// Run the tests
\Enhance\Core::runTests();