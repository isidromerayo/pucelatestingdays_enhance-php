<?php
namespace tests\unit\Agilecyl;

/**
 * Description of Demo
 *
 * @author isidromerayo
 */
class AreaTests extends \Enhance\TestFixture {

    private $area;

    public function setUp() {
        $this->area = \Enhance\Core::getCodeCoverageWrapper('\Agilecyl\Area');
    }

    function testTriangle() {
        $result = $this->area->triangle(6, 10);
        \Enhance\Assert::areIdentical(30, $result);
    }

    function testRectangle() {
        $area = new \Agilecyl\Area();
        $result = $area->rectangle(2, 5);
        \Enhance\Assert::areIdentical(10, $result);
    }

    function testSquare() {
        $area = new \Agilecyl\Area();
        $result = $area->square(4);
        \Enhance\Assert::isTrue(is_numeric($result));
    }

}
