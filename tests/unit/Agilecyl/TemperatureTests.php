<?php
namespace tests\unit\Agilecyl;

/**
 * Description of TemperatureTest
 *
 * @author isidromerayo
 */
class TemperatureTests extends \Enhance\TestFixture {
    
    public function testSimpleStubServiceTemperature()
    {
        
        $service = \Enhance\MockFactory::createMock('\Agilecyl\ServiceInterface');
        $service->addExpectation(
                \Enhance\Expect::method('readTemp')->returns(10)->returns(12)->returns(14)->times(3));
                
        $temperature = new \Agilecyl\Temperature($service);
        \Enhance\Assert::areIdentical(12, $temperature->average());
    }
}
