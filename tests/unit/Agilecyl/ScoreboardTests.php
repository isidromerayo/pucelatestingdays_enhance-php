<?php
namespace tests\unit\Agilecyl;

/**
 * Description of ScoreboardTest
 *
 * @author isidromerayo
 */
class ScoreboardTests extends \Enhance\TestFixture {
  public function score_home_calls_increment () {
    $home_counter_mock = \Enhance\MockFactory::createMock('\Agilecyl\Counter');
    $away_counter = new \Agilecyl\Counter();
 
    $home_counter_mock->addExpectation( \Enhance\Expect::method('increment') );
 
    $scoreboard = new \Agilecyl\Scoreboard($home_counter_mock, $away_counter);
    $scoreboard->score_home();
 
    $home_counter_mock->verifyExpectations();
  }
}
