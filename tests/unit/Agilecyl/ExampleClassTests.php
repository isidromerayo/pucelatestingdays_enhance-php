<?php
namespace tests\unit\Agilecyl;

/**
 * Description of ExampleClassTests
 *
 * @author isidromerayo
 */
class ExampleClassTests extends \Enhance\TestFixture
{
    public function testDummy() {
        \Enhance\Assert::areIdentical(2, 1 + 1);
        \Enhance\Assert::areNotIdentical("Nettuts+", "Psdtuts+");
    }
}